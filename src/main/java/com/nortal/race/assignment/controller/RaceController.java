package com.nortal.race.assignment.controller;

import com.google.inject.Inject;
import com.nortal.race.assignment.model.*;
import com.nortal.race.assignment.service.TargetService;
import com.nortal.race.assignment.service.VesselCommandService;

import java.awt.*;
import java.util.Optional;

public class RaceController {

    @Inject
    private TargetService targetService;

    @Inject
    private VesselCommandService vesselCommandService;

    private static final VesselCommand DEFAULT_THRUSTER_COMMAND = new VesselCommand(Thruster.BACK, ThrusterLevel.T0);

    public VesselCommand calculateNextCommand(Vessel vessel, RaceArea raceArea) {
        Point vesselPosition = vessel.getPosition();

        targetService.captureTargetsInRangeOfPosition(vesselPosition, raceArea.getTargets());

        Optional<Point> destinationPosition = targetService.getNextTargetClosestToPosition(vesselPosition);

        return destinationPosition.map(destination -> vesselCommandService.getCommandTowardsDestination(vessel, destination))
                .orElse(DEFAULT_THRUSTER_COMMAND);
    }

}

