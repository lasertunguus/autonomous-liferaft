package com.nortal.race.assignment.config;

public class RaceConfig {

    public static final int TARGET_CAPTURING_RANGE = 2;
    public static final double WATER_DRAG_DECELERATION = 1.6;

}
