package com.nortal.race.assignment.module;

import com.google.inject.AbstractModule;
import com.nortal.race.assignment.service.ThrusterLevelProjectionService;

public class CommandModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ThrusterLevelProjectionService.class);
    }

}
