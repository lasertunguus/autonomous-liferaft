package com.nortal.race.assignment.module;

import com.google.inject.AbstractModule;
import com.nortal.race.assignment.controller.RaceController;

public class MainModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new ControllerModule());
        bind(RaceController.class);
    }

}
