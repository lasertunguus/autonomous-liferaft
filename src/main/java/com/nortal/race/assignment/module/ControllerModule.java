package com.nortal.race.assignment.module;

import com.google.inject.AbstractModule;
import com.nortal.race.assignment.service.TargetService;
import com.nortal.race.assignment.service.VesselCommandService;

public class ControllerModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new CommandModule());
        bind(TargetService.class);
        bind(VesselCommandService.class);
    }

}
