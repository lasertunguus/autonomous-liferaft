package com.nortal.race.assignment.service;

import com.google.inject.Inject;
import com.nortal.race.assignment.model.Thruster;
import com.nortal.race.assignment.model.ThrusterLevel;
import com.nortal.race.assignment.model.Vessel;
import com.nortal.race.assignment.model.VesselCommand;

import java.awt.*;

public class VesselCommandService {

    @Inject
    private ThrusterLevelProjectionService distanceProjectionService;

    private Point vesselPosition;
    private Point destination;
    private double distanceToCoverOnAbscissa;
    private double distanceToCoverOnOrdinate;
    private double velocityX;
    private double velocityY;

    public VesselCommand getCommandTowardsDestination(Vessel vessel, Point destinationPosition) {
        setVariables(vessel, destinationPosition);

        if (distanceToCoverOnAbscissa > distanceToCoverOnOrdinate) {
            return getProjectingCommandOnAbscissa();
        }

        return getProjectingCommandOnOrdinate();
    }

    private void setVariables(Vessel vessel, Point destination) {
        vesselPosition = vessel.getPosition();
        this.destination = destination;
        distanceToCoverOnAbscissa = Math.abs(destination.getX() - vesselPosition.getX());
        distanceToCoverOnOrdinate = Math.abs(destination.getY() - vesselPosition.getY());
        velocityX = Math.abs(vessel.getSpeedX());
        velocityY = Math.abs(vessel.getSpeedY());
    }

    private VesselCommand getProjectingCommandOnAbscissa() {
        ThrusterLevel thrustLevel = distanceProjectionService.getHighestNotOvershootingThrust(distanceToCoverOnAbscissa, velocityX);
        return new VesselCommand(getThrusterOnAbscissa(), thrustLevel);
    }

    private VesselCommand getProjectingCommandOnOrdinate() {
        ThrusterLevel thrustLevel = distanceProjectionService.getHighestNotOvershootingThrust(distanceToCoverOnOrdinate, velocityY);
        return new VesselCommand(getThrusterOnOrdinate(), thrustLevel);
    }

    private Thruster getThrusterOnOrdinate() {
        Thruster thruster = Thruster.FRONT;
        if (destination.getY() > vesselPosition.getY()) {
            thruster = Thruster.BACK;
        }
        return thruster;
    }

    private Thruster getThrusterOnAbscissa() {
        Thruster thruster = Thruster.RIGHT;
        if (destination.getX() > vesselPosition.getX()) {
            thruster = Thruster.LEFT;
        }
        return thruster;
    }

}
