package com.nortal.race.assignment.service;

import com.nortal.race.assignment.model.ThrusterLevel;

import java.util.Map;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.nortal.race.assignment.config.RaceConfig.TARGET_CAPTURING_RANGE;
import static com.nortal.race.assignment.config.RaceConfig.WATER_DRAG_DECELERATION;
import static java.util.Arrays.stream;

public class ThrusterLevelProjectionService {

    ThrusterLevel getHighestNotOvershootingThrust(double distanceToCover, double velocity) {
        return projectDistanceCoveredWithAllThrustLevels(velocity).entrySet().stream()
                .filter(projection -> projection.getValue() < distanceToCover + TARGET_CAPTURING_RANGE)
                .map(Map.Entry::getKey).reduce((a, b) -> b).orElse(ThrusterLevel.T0);
    }

    private static TreeMap<ThrusterLevel, Double> projectDistanceCoveredWithAllThrustLevels(double speed) {
        return stream(ThrusterLevel.values())
                .filter(isThrusterLevelHigherThanZero())
                .filter(isResultingSpeedMoreThanOne(speed))
                .collect(Collectors.toMap(x -> x, thrusterLevel -> getDistanceCoveredWithGivenSpeed(thrusterLevel.getAcceleration() + speed),
                        (oldVal, newVal) -> newVal,
                        TreeMap::new));
    }

    private static Predicate<ThrusterLevel> isThrusterLevelHigherThanZero() {
        return thrusterLevel -> thrusterLevel != ThrusterLevel.T0;
    }

    private static Predicate<ThrusterLevel> isResultingSpeedMoreThanOne(double speed) {
        return thrusterLevel -> Math.abs(speed + thrusterLevel.getAcceleration() - 1.0) >= WATER_DRAG_DECELERATION;
    }

    private static Double getDistanceCoveredWithGivenSpeed(Double speed) {
        return 100 * Math.log((WATER_DRAG_DECELERATION + Math.pow(speed, 2) / 200) / WATER_DRAG_DECELERATION);
    }

}
