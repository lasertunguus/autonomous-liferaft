package com.nortal.race.assignment.service;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.nortal.race.assignment.config.RaceConfig.TARGET_CAPTURING_RANGE;

public class TargetService {

    private List<Point> targets = Collections.emptyList();
    private HashSet<Point> capturedTargets = new HashSet<>();

    public void captureTargetsInRangeOfPosition(Point position, List<Point> targets) {
        this.targets = targets;
        targets.stream().filter(isTargetInCapturingRange(position)).forEach(capturedTargets::add);
    }

    public Optional<Point> getNextTargetClosestToPosition(Point position) {
        return getTargetsToBeCaptured().sorted(compareByDistance(position)).findFirst();
    }

    private Comparator<Point> compareByDistance(Point position) {
        return Comparator.comparingDouble(point -> point.distance(position));
    }

    private Stream<Point> getTargetsToBeCaptured() {
        return targets.stream().filter(target -> !capturedTargets.contains(target));
    }

    private Predicate<Point> isTargetInCapturingRange(Point vesselPosition) {
        return target -> target.distance(vesselPosition.getX(), vesselPosition.getY()) <= TARGET_CAPTURING_RANGE;
    }

}
