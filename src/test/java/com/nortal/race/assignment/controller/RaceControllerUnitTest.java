package com.nortal.race.assignment.controller;

import com.nortal.race.assignment.model.RaceArea;
import com.nortal.race.assignment.model.ThrusterLevel;
import com.nortal.race.assignment.model.Vessel;
import com.nortal.race.assignment.model.VesselCommand;
import com.nortal.race.assignment.service.TargetService;
import com.nortal.race.assignment.service.VesselCommandService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.awt.*;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RaceControllerUnitTest {

    @InjectMocks
    private RaceController raceController;

    @Mock
    private TargetService targetService;

    @Mock
    private VesselCommandService vesselCommandService;

    private Point vesselPosition = new Point();
    private Vessel vessel = new Vessel();
    private RaceArea raceArea = new RaceArea();

    @Before
    public void setUp() {
        vessel.setPosition(vesselPosition);
    }

    @Test
    public void should_get_capture_targets_and_return_next_command() {
        Point target = new Point();
        when(targetService.getNextTargetClosestToPosition(vesselPosition)).thenReturn(Optional.of(target));
        VesselCommand nextCommand = mock(VesselCommand.class);
        when(vesselCommandService.getCommandTowardsDestination(vessel, target)).thenReturn(nextCommand);

        VesselCommand controllerCommand = raceController.calculateNextCommand(vessel, raceArea);

        assertEquals(nextCommand, controllerCommand);
        verify(targetService).captureTargetsInRangeOfPosition(vesselPosition, raceArea.getTargets());
    }

    @Test
    public void should_return_t0_thrust_level_command_when_no_targets_left() {
        when(targetService.getNextTargetClosestToPosition(vesselPosition)).thenReturn(Optional.empty());

        VesselCommand controllerCommand = raceController.calculateNextCommand(vessel, raceArea);

        assertEquals(ThrusterLevel.T0, controllerCommand.getThrusterLevel());
    }

}
