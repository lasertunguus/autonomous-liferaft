package com.nortal.race.assignment.service;

import com.nortal.race.assignment.model.ThrusterLevel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ThrusterLevelProjectionServiceUnitTest {

    private ThrusterLevelProjectionService thrusterLevelProjectionService = new ThrusterLevelProjectionService();

    @Test
    public void should_not_return_a_thrust_when_no_distance_to_cover() {
        assertEquals(ThrusterLevel.T0, thrusterLevelProjectionService.getHighestNotOvershootingThrust(0, 10));
    }

    @Test
    public void should_return_t2_thrust_to_cover_2m_from_velocity_of_1() {
        assertEquals(ThrusterLevel.T2, thrusterLevelProjectionService.getHighestNotOvershootingThrust(2, 1));
    }

    @Test
    public void should_return_t3_thrust_to_cover_1m_from_standstill() {
        assertEquals(ThrusterLevel.T3, thrusterLevelProjectionService.getHighestNotOvershootingThrust(1, 0));
    }

    @Test
    public void should_return_t4_thrust_to_cover_3m_from_standstill() {
        assertEquals(ThrusterLevel.T4, thrusterLevelProjectionService.getHighestNotOvershootingThrust(3, 0));
    }

    @Test
    public void should_return_highest_thrust_with_distance_left_to_cover() {
        assertEquals(ThrusterLevel.T4, thrusterLevelProjectionService.getHighestNotOvershootingThrust(5, 0));
    }

}