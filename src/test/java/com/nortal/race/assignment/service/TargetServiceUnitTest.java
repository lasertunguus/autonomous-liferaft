package com.nortal.race.assignment.service;

import org.junit.Test;

import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.nortal.race.assignment.config.RaceConfig.TARGET_CAPTURING_RANGE;
import static org.junit.Assert.*;

public class TargetServiceUnitTest {

    private TargetService targetService = new TargetService();

    @Test
    public void should_capture_targets_in_range() {
        Point position = getPoint(10, 10);
        Point firstTarget = getPoint(9, 9);
        Point secondTarget = getPoint(9, 9);
        Point thirdTarget = getPoint(11, 11);
        Point fourthTarget = getPoint(8, 10);
        List<Point> targets = Arrays.asList(firstTarget, secondTarget, thirdTarget, fourthTarget);

        targetService.captureTargetsInRangeOfPosition(position, targets);

        assertFalse(targetService.getNextTargetClosestToPosition(position).isPresent());
    }

    @Test
    public void should_not_capture_target_not_in_range_of_x_axis() {
        Point position = getPoint(10, 10);
        Point target = getPoint(position.getX() + TARGET_CAPTURING_RANGE + 1, position.getY());
        targetService.captureTargetsInRangeOfPosition(position, Collections.singletonList(target));

        Optional<Point> nextTarget = targetService.getNextTargetClosestToPosition(target);

        assertTrue(nextTarget.isPresent());
        assertEquals(target, nextTarget.get());
    }

    @Test
    public void should_not_capture_target_not_in_range_of_y_axis() {
        Point position = getPoint(10, 10);
        Point target = getPoint(position.getX(), position.getY() + TARGET_CAPTURING_RANGE + 1);
        targetService.captureTargetsInRangeOfPosition(position, Collections.singletonList(target));

        Optional<Point> nextTarget = targetService.getNextTargetClosestToPosition(target);

        assertTrue(nextTarget.isPresent());
        assertEquals(target, nextTarget.get());
    }

    private static Point getPoint(double x, double y) {
        return new Point((int) x, (int) y);
    }

}