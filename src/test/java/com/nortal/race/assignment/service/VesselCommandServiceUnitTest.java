package com.nortal.race.assignment.service;


import com.nortal.race.assignment.model.Thruster;
import com.nortal.race.assignment.model.ThrusterLevel;
import com.nortal.race.assignment.model.Vessel;
import com.nortal.race.assignment.model.VesselCommand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VesselCommandServiceUnitTest {

    @InjectMocks
    private VesselCommandService vesselCommandService;

    @Mock
    private ThrusterLevelProjectionService distanceProjectionService;

    private static final double SPEED_X = 3.0;
    private static final double SPEED_Y = 4.0;

    @Test
    public void should_use_left_thruster_on_abscissa() {
        Vessel vessel = getVesselWithPosition(new Point(0, 0));
        Point destination = new Point(1, 0);
        when(distanceProjectionService.getHighestNotOvershootingThrust(eq(1.0, 1e-5), eq(SPEED_X, 1e-5))).thenReturn(ThrusterLevel.T2);

        VesselCommand commandTowardsDestination = vesselCommandService.getCommandTowardsDestination(vessel, destination);

        assertEquals(Thruster.LEFT, commandTowardsDestination.getThruster());
        assertEquals(ThrusterLevel.T2, commandTowardsDestination.getThrusterLevel());
    }

    @Test
    public void should_use_right_thruster_on_abscissa() {
        Vessel vessel = getVesselWithPosition(new Point(1, 0));
        Point destination = new Point(0, 0);
        when(distanceProjectionService.getHighestNotOvershootingThrust(eq(1.0, 1e-5), eq(SPEED_X, 1e-5))).thenReturn(ThrusterLevel.T3);

        VesselCommand commandTowardsDestination = vesselCommandService.getCommandTowardsDestination(vessel, destination);

        assertEquals(Thruster.RIGHT, commandTowardsDestination.getThruster());
        assertEquals(ThrusterLevel.T3, commandTowardsDestination.getThrusterLevel());
    }

    @Test
    public void should_use_back_thruster_on_ordinate() {
        Vessel vessel = getVesselWithPosition(new Point(0, 0));
        Point destination = new Point(0, 1);
        when(distanceProjectionService.getHighestNotOvershootingThrust(eq(1.0, 1e-5), eq(SPEED_Y, 1e-5))).thenReturn(ThrusterLevel.T4);

        VesselCommand commandTowardsDestination = vesselCommandService.getCommandTowardsDestination(vessel, destination);

        assertEquals(Thruster.BACK, commandTowardsDestination.getThruster());
        assertEquals(ThrusterLevel.T4, commandTowardsDestination.getThrusterLevel());
    }

    @Test
    public void should_use_front_thruster_on_ordinate() {
        Vessel vessel = getVesselWithPosition(new Point(0, 1));
        Point destination = new Point(0, 0);
        when(distanceProjectionService.getHighestNotOvershootingThrust(eq(1.0, 1e-5), eq(SPEED_Y, 1e-5))).thenReturn(ThrusterLevel.T4);

        VesselCommand commandTowardsDestination = vesselCommandService.getCommandTowardsDestination(vessel, destination);

        assertEquals(Thruster.FRONT, commandTowardsDestination.getThruster());
        assertEquals(ThrusterLevel.T4, commandTowardsDestination.getThrusterLevel());
    }

    private static Vessel getVesselWithPosition(Point position) {
        Vessel vessel = new Vessel();
        vessel.setSpeedX(SPEED_X);
        vessel.setSpeedY(SPEED_Y);
        vessel.setPosition(position);
        return vessel;
    }

}
